Українська локалізація v1.0 для Bannerlord 1.6.2 ⚔ Ukrainian Localization v1.0 for Bannerlord 1.6.2

❗️Ручний переклад ще не повний. Десь 80%, може трохи меньше.❗️

Встановлення:
- Розпакувати архів в папку "Modules". Повинно бути так - Modules/Ukrainization
- При запуску гри (в лаунчері), перейти на вкладку "Mods" та поставити галочку на моді "Ukrainization by UnDenya"
- В грі, в налаштуваннях, обрати мову "Українська"

Facebook сторінка потребує підписників 😉 
https://www.facebook.com/vukrainization

Також можете пригостити автора філіжаночкою кави:

На патреоні:
https://www.patreon.com/vukrainization

Або одноразово:
https://vukrainization.diaka.ua/donate